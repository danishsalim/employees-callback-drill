function removeEntry(id, data, cb) {
  try {
    const updatedData = data["employees"].filter((eachdata) => {
      return eachdata.id != id;
    });
    cb(null, updatedData);
  } catch (error) {
    cb(error, null);
  }
}

module.exports = { removeEntry };
