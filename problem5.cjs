function sortedCompanyName(data, cb) {
  try {
    data["employees"].sort((employee1, employee2) => {
      if (employee1.company > employee2.company) {
        return 1;
      } else if (employee1.company < employee2.company) {
        return -1;
      } else {
        if (employee1.id > employee2.id) {
          return 1;
        } else if (employee1.id < employee2.id) {
          return -1;
        } else {
          return 0;
        }
      }
    });
    cb(null, data);
  } catch (error) {
    cb(error, null);
  }
}

module.exports = { sortedCompanyName };
