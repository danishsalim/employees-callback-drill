function swapPosition(id1, id2, data, cb) {
  try {
    dataOfId1 = data["employees"].find((employee) => {
      return employee.id == id1;
    });
    dataOfId2 = data["employees"].find((employee) => {
      return employee.id == id2;
    });
    const dataWithSwap = data["employees"].map((employee) => {
      if (employee.id == id1) {
        return dataOfId2;
      } else if (employee.id == id2) {
        return dataOfId1;
      } else {
        return employee;
      }
    });
    cb(null, dataWithSwap);
  } catch (error) {
    cb(error, null);
  }
}

module.exports = { swapPosition };
