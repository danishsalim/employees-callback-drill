const fs = require("fs");
const { retrieveData } = require("./problem1.cjs");
const { groupData } = require("./problem2.cjs");
const { powerpuff } = require("./problem3.cjs");
const { removeEntry } = require("./problem4.cjs");
const { sortedCompanyName } = require("./problem5.cjs");
const { swapPosition } = require("./problem6.cjs");
const { addBirthdayToEvenId } = require("./problem7.cjs");

fs.readFile("./data.json", "utf-8", (error, data) => {
  try {
    if (error) {
      return console.log("error in reading file");
    } else {
      data = JSON.parse(data);
      retrieveData([2, 13, 23], data, (error, dataOfIds) => {
        if (error) {
          return console.log("error in retriveData");
        } else {
          fs.writeFile(
            "./output/dataOfIds.json",
            JSON.stringify(dataOfIds),
            (error) => {
              if (error) {
                return console.log("error in writing dataOfIds");
              }
            }
          );
          groupData(data, (error, dataOfEachCompany) => {
            if (error) {
              return console.log("error in groupData function");
            } else {
              fs.writeFile(
                "./output/companyWiseData.json",
                JSON.stringify(dataOfEachCompany),
                (error) => {
                  if (error) {
                    return console.log("error in writing dataOfEachCompany");
                  }
                }
              );
              powerpuff(dataOfEachCompany, (error, dataOfPowerpuff) => {
                if (error) {
                  return console.log("error in powerpuff");
                } else {
                  fs.writeFile(
                    "./output/powerpuff.json",
                    JSON.stringify(dataOfPowerpuff),
                    (error) => {
                      if (error) {
                        throw error;
                      }
                    }
                  );
                  removeEntry(2, data, (error, updatedData) => {
                    if (error) {
                      return console.log("error in removeEntry function");
                    } else {
                      fs.writeFile(
                        "./output/updatedData.json",
                        JSON.stringify(updatedData),
                        (error) => {
                          if (error) {
                            return console.log(error);
                          }
                        }
                      );
                      sortedCompanyName(data, (error, sortedData) => {
                        if (error) {
                          return console.log(
                            "error in sortedCompanyName function"
                          );
                        } else {
                          fs.writeFile(
                            "./output/sortedData.json",
                            JSON.stringify(sortedData),
                            (error) => {
                              if (error) {
                                return console.log(error);
                              }
                            }
                          );
                          swapPosition(92, 93, data, (error, swappedData) => {
                            if (error) {
                              return console.log(error);
                            } else {
                              fs.writeFile(
                                "./output/swappedData.json",
                                JSON.stringify(swappedData),
                                (error) => {
                                  if (error) {
                                    return console.log(error);
                                  }
                                }
                              );
                              addBirthdayToEvenId(
                                data,
                                (error, birthdayAddedData) => {
                                  if (error) {
                                    return console.log(error);
                                  } else {
                                    fs.writeFile(
                                      "./output/birthdayAddedData.json",
                                      JSON.stringify(birthdayAddedData),
                                      (error) => {
                                        if (error) {
                                          return console.log(error);
                                        }
                                      }
                                    );
                                  }
                                }
                              );
                            }
                          });
                        }
                      });
                    }
                  });
                }
              });
            }
          });
        }
      });
    }
  } catch (error) {
    console.log(error);
  }
});
