function addBirthdayToEvenId(data, cb) {
  try {
    let birthdayAddedData = data["employees"].map((employee) => {
      let date = new Date();
      let dateString =
        date.getFullYear() + "/" + (date.getMonth() + 1) + "/" + date.getDate();
      if (employee.id % 2 == 0) {
        employee["birthday"] = dateString;
      }
      return employee;
    });
    cb(null, birthdayAddedData);
  } catch (error) {
    cb(error, null);
  }
}

module.exports = { addBirthdayToEvenId };
