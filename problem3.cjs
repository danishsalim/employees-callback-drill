function powerpuff(dataOfEachCompany, cb) {
  try {
    let dataOfPowerpuff = dataOfEachCompany["Powerpuff Brigade"];
    cb(null, dataOfPowerpuff);
  } catch (error) {
    cb(error, null);
  }
}

module.exports = { powerpuff };
