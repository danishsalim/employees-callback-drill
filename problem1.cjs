function retrieveData(ids, data, cb) {
  try {
    const dataOfIds = data["employees"].filter((individual) => {
      return ids.includes(individual.id);
    });
    cb(null, dataOfIds);
  } catch (error) {
    cb(error, null);
  }
}

module.exports = { retrieveData };
