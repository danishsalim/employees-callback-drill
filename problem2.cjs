function groupData(data, cb) {
  try {
    let group = data["employees"].reduce((employees, employee) => {
      if (employees[employee.company]) {
        employees[employee.company].push(employee);
      } else {
        employees[employee.company] = [employee];
      }
      return employees;
    }, {});
    cb(null, group);
  } catch (error) {
    cb(error, null);
  }
}

module.exports = { groupData };
